/*************************************************************************
    > File Name: tcp_funcinit.h
    > Author: JiaMing
    > Mail: day961@gmail.com 
    > Created Time: 2014年10月23日 星期四 17时07分07秒
 ************************************************************************/

#ifndef __FUNCINIT_HEAD
#define __FUNCINIT_HEAD

#define DEFAULT_PORT 8000

int socket_init(int *sockfd);

#endif

