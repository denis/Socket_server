/*************************************************************************
    > File Name: pthread_func.h
    > Author: JiaMing
    > Mail: day961@gmail.com 
    > Created Time: 2014年10月24日 星期五 11时28分45秒
 ************************************************************************/

#ifndef __PTHREAD_FUNC_HEAD
#define __PTHREAD_FUNC_HEAD

void *server_thread();

#endif

