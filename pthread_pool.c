/*******************************************
pthread_pool.c
*******************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#include <assert.h>
#include "pthread_pool.h"


//share resource
static CThread_pool *pool = NULL;

/*线程池初始化*/
void tpool_thread_init (int max_thread_num)
{
    int i = 0;
    pool = (CThread_pool *) malloc (sizeof (CThread_pool));
    pthread_mutex_init (&(pool->queue_lock), NULL);
    pthread_cond_init (&(pool->queue_ready), NULL);

    pool->queue_head = NULL;
    pool->max_thread_num = max_thread_num;
    pool->cur_queue_size = 0;
    pool->shutdown = 0;
    //动态创建线程号
    pool->threadid = (pthread_t *) malloc (max_thread_num * sizeof (pthread_t));

    for (i = 0; i < max_thread_num; i++){ 
        pthread_create (&(pool->threadid[i]), NULL, tpool_thread_routine,NULL);
    }
}

/*向线程池中加入任务*/
int tpool_add_worker (void *(*process) (void *arg), void *arg)
{
    /*构造一个新任务节点*/
    CThread_worker *newworker = (CThread_worker *) malloc (sizeof (CThread_worker));
    newworker->process = process;
    newworker->arg = arg;
    newworker->next = NULL;
    
    pthread_mutex_lock (&(pool->queue_lock));
    
    /*将任务节点加入到等待队列中*/
    CThread_worker *member = pool->queue_head;
    if (member != NULL)
    {
        while (member->next != NULL)
            member = member->next;
        member->next = newworker;
    }
    else
    {
        pool->queue_head = newworker;
    }

    assert (pool->queue_head != NULL);

    pool->cur_queue_size++;
    pthread_mutex_unlock (&(pool->queue_lock));
    /*好了，等待队列中有任务了，唤醒一个等待线程；
    注意如果所有线程都在忙碌，这句没有任何作用*/
    pthread_cond_signal (&(pool->queue_ready));
    return 0;
}

/*线程任务*/
static void *tpool_thread_routine (void *arg)
{
    printf ("starting thread 0x%x\n", pthread_self());
    while (1)
    {
        pthread_mutex_lock (&(pool->queue_lock));
        
        /*如果等待队列为0并且不销毁线程池，则处于阻塞状态; 注意
            pthread_cond_wait是一个原子操作，等待前会解锁，唤醒后会加锁*/
        while (pool->cur_queue_size == 0 && !pool->shutdown){
            printf ("thread 0x%x is waiting\n", pthread_self());
            pthread_cond_wait (&(pool->queue_ready), &(pool->queue_lock));
        }

        /*线程池要销毁了*/
        if (pool->shutdown)
        {
            /*遇到break,continue,return等跳转语句，千万不要忘记先解锁*/
            pthread_mutex_unlock (&(pool->queue_lock));
            printf("thread 0x%x will exit\n", pthread_self ());
            pthread_exit (NULL);
        }

        printf ("thread 0x%x is starting to work\n", pthread_self ());

        /*assert是调试的好帮手*/
        assert (pool->cur_queue_size != 0);
        assert (pool->queue_head != NULL);
        
        /*等待队列长度减1*/
        pool->cur_queue_size--;
        //并取出链表中的头元素
        CThread_worker *worker = pool->queue_head;
        //线程池等待任务列表指针后移一位
        pool->queue_head = worker->next;
        pthread_mutex_unlock (&(pool->queue_lock));

        /*调用回调函数，执行任务*/
        (*(worker->process)) (worker->arg);
        free(worker);
        worker = NULL;
    }
    
    pthread_exit (NULL);//never used
}

/*销毁线程池，等待队列中的任务不会再被执行，但是正在运行的线程会一直
把任务运行完后再退出*/
int tpool_thread_destroy ()
{
    int i;
    
    if (pool->shutdown)
        return -1;/*防止两次调用*/
    pool->shutdown = 1;

    /*唤醒所有等待线程，线程池要销毁了*/
    pthread_cond_broadcast (&(pool->queue_ready));

    /*阻塞等待线程退出，否则就成僵尸了*/
    for (i = 0; i < pool->max_thread_num; i++){
        pthread_join(pool->threadid[i], NULL);
    }
    free(pool->threadid);

    /*销毁等待队列*/
    CThread_worker *head = NULL;
    while (pool->queue_head != NULL){
        head = pool->queue_head;
        pool->queue_head = pool->queue_head->next;
        free (head);
    }
    
    /*销毁条件变量和互斥量*/
    pthread_mutex_destroy(&(pool->queue_lock));
    pthread_cond_destroy(&(pool->queue_ready));
    
    free (pool);
    pool = NULL;
    return 0;
}

